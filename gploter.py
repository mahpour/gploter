from BCBio import GFF
import cairo
from collections import defaultdict
import re


chrs = defaultdict(list)

in_file = "gencode.v19.annotation.gff3"

limit_info = dict(
        gff_source_type = [('ENSEMBL', 'gene')])

in_handle = open(in_file)
rec = GFF.parse(in_handle, limit_info=limit_info)

for i in rec:
    plus = []
    minus = []
    for j in i.features:
        if j.location._strand == 1:
            plus.append([int(j.location._start),int(j.location._end)])
        else:
            minus.append([int(j.location._start),int(j.location._end)])

    chrs[i.id].append([plus, minus])
in_handle.close()


def draw_gene(i,x):
    context.set_source_rgb(*color)
    #rect = [50 + i[0] / scale, x, (i[1] / scale - i[0] / scale) * 10, height]
    rect = [50 + i[0] / scale, x, 0.5, height]
    context.rectangle(*rect)
    context.fill()

# THE FOLLOWING FUNCTION WAS ADOPTED FROM https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/
# THANKS JEFF!

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)


with cairo.PDFSurface("s1.pdf", 1200, len(chrs.keys())*25+100) as surface:
#with cairo.ImageSurface(cairo.FORMAT_ARGB32, 1200, len(chrs.keys())*25+100) as surface:
    context = cairo.Context(surface)

    context.scale(1, 1)
    context.set_antialias(cairo.ANTIALIAS_NONE)
    scale =300000.0
    height=10
    color = [0.8,0.8,0.8]
    context.set_source_rgb(1,1,1)
        #context.rectangle()

    context.rectangle(0,0,1200,800)
    context.fill()
    no = 0

    chroms = [i for i in chrs.keys()]
    sorted_chrs = natural_sort(chroms)
    #print(l)

    for chrom in sorted_chrs:
        print(chrom)
        no += 1
        x = (no * 25) + 10
        context.set_source_rgb(0, 0, 0)
        context.move_to(10, x+height+height/4)
        context.show_text(chrom)

        context.fill()

        for i in chrs[chrom][0][0]:
            #print(i)
            context.set_source_rgb(*color)
            #context.rectangle()

            draw_gene(i,x)

        for i in chrs[chrom][0][1]:
            context.set_source_rgb(*color)

            draw_gene(i,x+height)

    #surface.write_to_png("example.png")


